class Category < ApplicationRecord
  has_many :categoryposts
  has_many :posts, through: :categoryposts
end
