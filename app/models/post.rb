class Post < ApplicationRecord
  has_many :categoryposts
  has_many :categories, through: :categoryposts
end
