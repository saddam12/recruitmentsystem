class CreateCategoryposts < ActiveRecord::Migration[5.1]
  def change
    create_table :categoryposts do |t|
      t.belongs_to :post, index: true
      t.belongs_to :category, index: true
      t.timestamps
    end
  end
end
