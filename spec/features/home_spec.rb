require "rails_helper"

RSpec.feature "home page", :js => true,:type => :feature do
  scenario "User access home  page" do
    visit root_path
    expect(page).to have_text("Home Page")
    
  end
end